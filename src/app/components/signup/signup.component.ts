import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import {
  ReactiveFormsModule,
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
  EmailValidator
} from "@angular/forms";
import { NgForm } from "@angular/forms";
import { AngularFireDatabase } from "angularfire2/database";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  emailSignup: string;
  passwordSignup: string;
  constructor(
    private db: AngularFireDatabase,
    private fb: FormBuilder,
    private auth: AuthService
  ) {}

  ngOnInit(): void {
  }

  signup(data: NgForm): void {
    this.auth.emailSignUp(
      data.value.emailSignup,
      data.value.passwordSignup
    );
    this.db.list("/users").push(data.value);
  }
}
